//
//  rule_DependencyInversion.m
//  designPattern
//
//  Created by nimo on 2017/10/20.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "rule_DependencyInversion.h"

@implementation rule_DependencyInversion

@end

@implementation TXTDataConvertor
- (void) readFile {
    NSLog(@"read file from text");
}
@end

@implementation ExcelDataConvertor
- (void) readFile {
    NSLog(@"read file from excel");
}
@end

@implementation CustomerDAO

- (void)addCustomer {
    NSLog(@"read file from txtDataConvertor");
//    if the requirement changed, we need to change this line.
}

@end


//==========================================

@implementation TXTDataConvertor_refactor

- (void) readFile {
    NSLog(@"read file from txt");
}

@end

@implementation ExcelDataConvertor_refactor

- (void) readFile {
    NSLog(@"read file from");
}

@end

@implementation CustomerDAO_refactor

- (void)addCustomer:(id<DataConvertorProtocol>)customer {
    [customer readFile];
    NSLog(@"read file and add customer");
    
//    for usage
    /*
    CustomerDAO_refactor *customerDAO = [[CustomerDAO_refactor alloc] init];
    TXTDataConvertor_refactor *txtConvertor = [[TXTDataConvertor_refactor alloc] init];
    
    [customerDAO addCustomer: txtConvertor];
     */
}
@end

