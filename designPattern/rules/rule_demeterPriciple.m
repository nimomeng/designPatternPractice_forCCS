//
//  rule_demeterPriciple.m
//  designPattern
//
//  Created by nimo on 2017/10/24.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "rule_demeterPriciple.h"

@implementation rule_demeterPriciple

@end

@implementation UIComponent1
- (void)biz
{
    NSLog(@"talk to UIComponent3 directly");
}
@end


@implementation UIComponent2
- (void)biz
{
    NSLog(@"talk to UIComponent5 directly");
}
@end

@implementation UIComponent3
- (void)biz
{
    NSLog(@"talk to UIComponent6 directly");
}
@end

@implementation UIComponent4
- (void)biz
{
    NSLog(@"talk to UIComponent2 directly");
}
@end

@implementation UIComponent5
- (void)biz
{
    NSLog(@"talk to UIComponent1 directly");
}
@end

@implementation UIComponent6
- (void)biz
{
    NSLog(@"talk to UIComponent4 directly");
}
@end

//====================================================

@implementation mediator

- (void)biz
{
    NSLog(@"Do the cooperation role between components if necessary");
}

@end
