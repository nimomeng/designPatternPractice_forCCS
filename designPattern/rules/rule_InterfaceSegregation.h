//
//  rule_InterfaceSegregation.h
//  designPattern
//
//  Created by nimo on 2017/10/24.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_InterfaceSegregation : NSObject

@end

@protocol CustomerDataDisplay <NSObject>

- (void)dataRead;
- (void)transformToXML;
- (void)createChart;
- (void)displayChart;
- (void)createReport;
- (void)displayReport;

@end

@interface ConcreteClass <CustomerDataDisplay>

- (void)dataRead;
- (void)transformToXML;
- (void)createChart;
- (void)displayChart;
- (void)createReport;
- (void)displayReport;

@end

@interface ClientSide

// some method to involk concreteClass directly

@end

//=======================================================================

@protocol DataHandler <NSObject>
- (void)dataRead;
@end

@protocol XMLTransformer <NSObject>
- (void)transformToXML;
@end

@protocol ChartHandler <NSObject>
- (void)createChart;
- (void)displayChart;
@end

@protocol ReportHandler <NSObject>
- (void)createReport;
- (void)displayReport;
@end


@interface ConcreteClass_refactor <DataHandler,ChartHandler>
- (void)dataRead;
- (void)createChart;
- (void)displayChart;
@end

