//
//  rule_DependencyInversion.h
//  designPattern
//
//  Created by nimo on 2017/10/20.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_DependencyInversion : NSObject

@end


@interface TXTDataConvertor : NSObject

- (void)readFile;

@end

@interface ExcelDataConvertor : NSObject

- (void)readFile;

@end

@interface CustomerDAO : NSObject

- (void)addCustomer;

@end

//=========================================================

@protocol DataConvertorProtocol <NSObject>

@required
- (void)readFile;

@end


@interface TXTDataConvertor_refactor : NSObject <DataConvertorProtocol>

@end

@interface ExcelDataConvertor_refactor : NSObject <DataConvertorProtocol>

@end

@interface CustomerDAO_refactor : NSObject
- (void) addCustomer: (id<DataConvertorProtocol>)customer;
@end



