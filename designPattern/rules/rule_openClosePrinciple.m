//
//  rule_openClosePrinciple.m
//  designPattern
//
//  Created by nimo on 2017/10/19.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "rule_openClosePrinciple.h"

@implementation rule_openClosePrinciple

@end

@implementation ChartDisplay

- (void)display:(NSString *)type {
    if ([type isEqualToString:@"pie"]) {
        NSLog(@"balabala");
    }
    else if ([type isEqualToString:@"bar"]) {
        NSLog(@"balabala");
    }
//    add sth. for line type
}
@end


// Solution 1
@implementation ChartDisplayABS
- (void) display {
    NSLog(@"common operations");
}
@end

@implementation PieDisplay
- (void) display {
    [super display];
    
    NSLog(@"Do additional things for Pie");
}

@end

@implementation BarDisplay
- (void) display {
    [super display];
    
    NSLog(@"Do additional things for Bar");
}

@end

@implementation ChartDisplay_refactor

- (void) setChartType:(NSString *)type {
    if ([type isEqualToString: @"pie"]) {
        self.chart = [[PieDisplay alloc] init];
    }
    else if ([type isEqualToString: @"bar"]) {
        self.chart = [[BarDisplay alloc] init];
    }
}

- (void) display {
    [self.chart display];
}

@end

//Solution 2
@interface PieDisplay_Interface()<ChartDisplayProtocol>

@end

@implementation PieDisplay_Interface

- (void) display {
    NSLog(@"Do something for Pie here");
}

@end

@interface BarDisplay_Interface()<ChartDisplayProtocol>
@end

@implementation BarDisplay_Interface

- (void) display {
    NSLog(@"Do something for Bar here");
}

@end








