//
//  CompositeAggregateReusePrinciple.h
//  designPattern
//
//  Created by nimo on 2017/10/24.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_CompositeAggregateReusePrinciple : NSObject

@end


@interface DBUtil: NSObject
- (void)getConnection;
@end

@interface CustomerDAO_CARP: NSObject
- (void)addCustomer;
@end

//======================================

@interface DBUtil_refactor: NSObject
- (void)getConnection;
@end

@interface OracleDBUtil: DBUtil_refactor
@end

@interface CustomerDAO_CARP_refactor: NSObject
@property (nonatomic, strong) DBUtil_refactor *dbUtil;
- (void)addCustomer;
@end



