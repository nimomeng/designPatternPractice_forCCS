//
//  rule_openClosePrinciple.h
//  designPattern
//
//  Created by nimo on 2017/10/19.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_openClosePrinciple : NSObject

@end

@interface ChartDisplay : NSObject

- (void) display:(NSString *)type;

@end

//==========================================

// Solution 1: Abstract Layer

@interface ChartDisplayABS : NSObject
- (void) display;
@end

@interface ChartDisplay_refactor : NSObject

@property (nonatomic, strong) ChartDisplayABS *chart;

- (void) setChartType:(NSString *)type;
- (void) display;

@end

@interface PieDisplay : ChartDisplayABS

@end

@interface BarDisplay : ChartDisplayABS

@end

// Solution 2: Interface based programming

@protocol ChartDisplayProtocol <NSObject>

@required
- (void) display;

@end

@interface PieDisplay_Interface: NSObject

@end

@interface BarDisplay_Interface: NSObject

@end

