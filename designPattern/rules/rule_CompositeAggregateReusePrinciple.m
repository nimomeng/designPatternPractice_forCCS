//
//  CompositeAggregateReusePrinciple.m
//  designPattern
//
//  Created by nimo on 2017/10/24.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "rule_CompositeAggregateReusePrinciple.h"

@implementation rule_CompositeAggregateReusePrinciple

@end


@implementation DBUtil
- (void)getConnection
{
    NSLog(@"connect to some kind of DataBase");
}
@end

@implementation CustomerDAO_CARP

- (void) addCustomer {
//    what happended if we need to change database from MySQL to Oracle? change source code of DBUtil?
    DBUtil *dbUtil = [[DBUtil alloc] init];
    [dbUtil getConnection];
}

@end


//==================================

@implementation DBUtil_refactor
- (void)getConnection
{
    NSLog(@"need to be override");
}
@end


@implementation OracleDBUtil
- (void)getConnection
{
    [super getConnection];
    NSLog(@"do something on Oracle");
}
@end


@implementation CustomerDAO_CARP_refactor

- (void)addCustomer
{
//    use Dependency Inversion principle
    OracleDBUtil *oracleDBUtil = [[OracleDBUtil alloc] init];
    self.dbUtil = oracleDBUtil;
    
    NSLog(@"do something based on OracleDBUtil");
}

@end
