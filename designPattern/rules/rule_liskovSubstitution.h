//
//  rule_liskovSubstitution.h
//  designPattern
//
//  Created by nimo on 2017/10/20.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_liskovSubstitution : NSObject

@end


@interface EmailSender
- (void) sendEmailToCustomer;
- (void) sendEmailToVIPCustomer;
@end

@interface CommonCustomer
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

- (NSString *) getCustomerName;
- (void) setCustomerName:(NSString *)name;
- (NSString *) getCustomerEmail;
- (void) setCustomerEmail:(NSString *)email;
@end

@interface VIPCustomer
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

- (NSString *) getCustomerName;
- (void) setCustomerName:(NSString *)name;
- (NSString *) getCustomerEmail;
- (void) setCustomerEmail:(NSString *)email;
@end

//==========================================

@interface Customer

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

- (NSString *) getCustomerName;
- (void) setCustomerName:(NSString *)name;
- (NSString *) getCustomerEmail;
- (void) setCustomerEmail:(NSString *)email;
@end

@interface EmailSender_refactor
- (void)sendEmail:(Customer *)customer;
@end

@interface CommonCustomer_refactor : Customer

@end

@interface VIPCustomer_refactor : Customer

@end
