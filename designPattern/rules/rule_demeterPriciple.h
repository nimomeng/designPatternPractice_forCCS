//
//  rule_demeterPriciple.h
//  designPattern
//
//  Created by nimo on 2017/10/24.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_demeterPriciple : NSObject

@end

@interface UIComponent1: NSObject
- (void)biz;
@end

@interface UIComponent2: NSObject
- (void)biz;
@end

@interface UIComponent3: NSObject
- (void)biz;
@end

@interface UIComponent4: NSObject
- (void)biz;
@end

@interface UIComponent5: NSObject
- (void)biz;
@end

@interface UIComponent6: NSObject
- (void)biz;
@end


//===============================================

@interface mediator: NSObject
- (void)biz;
@end

