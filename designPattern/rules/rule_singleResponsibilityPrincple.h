//
//  rule_singleResponsibilityPrincple.h
//  designPattern
//
//  Created by nimo on 2017/10/19.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rule_singleResponsibilityPrincple : NSObject

@end

@interface CustomerDataChart_original : NSObject

//for operation
- (BOOL) getConnection;
- (NSObject *) findCustomer;

//for biz
- (BOOL) createChart;
- (void) displayChart;

@end

//========================================================


@interface DataBaseUtil : NSObject
+ (BOOL) getConnection;
- (NSObject *)selectDatabaseFrom: (NSString *)sqlString;
@end

@interface CustomerDataAccess : NSObject
@property (strong,nonatomic) DataBaseUtil *databaseUtil;
- (NSObject *) findCustomer;
@end
//- (void*) qdPort NS_RETURNS_INNER_POINTER NS_DEPRECATED_MAC(10_0, 10_4);

@interface CustomerDataChart_refactored : NSObject
- (CustomerDataAccess *) customerDataAccess;
- (BOOL) createChart;
- (void) displayChart;
@end




