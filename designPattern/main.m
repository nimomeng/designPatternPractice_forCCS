//
//  main.m
//  designPattern
//
//  Created by nimo on 2017/10/19.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrototypePattern.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        PrototypePattern *obj = [[PrototypePattern alloc] init];
        obj.address = @"test address";
        obj.size = CGSizeMake(100, 100);
        obj.age = 20;
        obj.students = @[@"zhang san",@"li si",@"wang wu"];
        
        NSLog(@"o:%@, array: %p",obj,obj.students);
        
        PrototypePattern *shallow_Obj = obj;
        PrototypePattern *deep_Obj = [obj copy];
        NSLog(@"s:%@ , array: %p",shallow_Obj, shallow_Obj.students);
        NSLog(@"d:%@ , array: %p",deep_Obj, deep_Obj.students);
    }
    return 0;
}
