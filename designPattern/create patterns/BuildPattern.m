//
//  BuildPattern.m
//  designPattern
//
//  Created by nimo on 2017/10/30.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "BuildPattern.h"

@implementation BuildPattern

@end


@implementation Director
- (void)construct:(id<Builder>)builder
{
//    id<ProductA> productA = [builder buildPartA];
//    id<ProductB> productB = [builder buildPartB];

//    ComplexObject *complexObj = [[ComplexObject alloc] init];
//    [complexObj constructWith:productA ProductB:productB];
}
@end





