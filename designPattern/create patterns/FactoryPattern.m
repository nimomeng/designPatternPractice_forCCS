//
//  FactoryPattern.m
//  designPattern
//
//  Created by nimo on 2017/10/25.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "FactoryPattern.h"

@implementation FactoryPattern

@end

@implementation ConcreteFactory1
- (void)createProductA
{
//    Product A logic for Factory1
    ProductA2 *productA = [[ProductA2 alloc] init];
}

- (void)createProductB
{
//    Product B logic for Factory1
    ProductB2 *productB = [[ProductB2 alloc] init];

}
@end

@implementation ConcreteFactory2

- (void)createProductA
{
//    Product A logic for Factory2
    ProductA1 *productA = [[ProductA1 alloc] init];
}

- (void)createProductB
{
//    Product B logic for Factory2
    ProductB1 *productB = [[ProductB1 alloc] init];

}

@end

@implementation ProductA1
- (void)doSomething
{
//    do something for ProductA1
}
@end

@implementation ProductA2
- (void)doSomething
{
//    do something for ProductA2
}
@end

@implementation ProductB1
- (void)dosomething
{
//    do something for ProductB1
}
@end

@implementation ProductB2
- (void)dosomething
{
//    do something for ProductB2
}
@end




