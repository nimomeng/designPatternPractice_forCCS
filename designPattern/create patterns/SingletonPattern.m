//
//  SingletonPattern.m
//  designPattern
//
//  Created by nimo on 2017/10/26.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "SingletonPattern.h"

@implementation SingletonPattern
static SingletonPattern *instance = nil;
+ (instancetype)getInstance
{
    static dispatch_once_t oncetoken;
    dispatch_once(&oncetoken, ^{
        instance = [[SingletonPattern alloc] init];
    });
    return instance;
}
@end
