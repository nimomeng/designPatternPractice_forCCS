//
//  BuildPattern.h
//  designPattern
//
//  Created by nimo on 2017/10/30.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuildPattern : NSObject
@end

@protocol ProductA<NSObject>
- (instancetype)createA;
@end

@protocol ProductB<NSObject>
- (instancetype)createB;
@end


@interface ProductA1: NSObject<ProductA>
@end

@interface ProductB1: NSObject<ProductB>
@end

@protocol Builder<NSObject>
- (id<ProductA>)buildPartA;
- (id<ProductB>)buildPartB;
@end

@interface Builder1: NSObject<Builder>
@end

@interface Director: NSObject
- (void)construct:(id<Builder>) builder;
@end

@interface ComplexObject: NSObject
- (void)constructWith:(id<ProductA>)productA ProductB:(id<ProductB>)productB;
@end




