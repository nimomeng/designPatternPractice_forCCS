//
//  FactoryPattern.h
//  designPattern
//
//  Created by nimo on 2017/10/25.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FactoryPattern : NSObject

@end

@interface Client: NSObject

@end

@protocol AbstractFactory<NSObject>

- (void)createProductA;
- (void)createProductB;

@end

@interface ConcreteFactory1: NSObject<AbstractFactory>

@end

@interface ConcreteFactory2: NSObject<AbstractFactory>

@end


@protocol AbstractProductA<NSObject>
- (void)doSomething;
@end

@interface ProductA1: NSObject<AbstractProductA>

@end

@interface ProductA2: NSObject<AbstractProductA>

@end

@protocol AbstractProductB<NSObject>
- (void)dosomething;
@end

@interface ProductB1: NSObject<AbstractProductB>

@end

@interface ProductB2: NSObject<AbstractProductB>

@end



