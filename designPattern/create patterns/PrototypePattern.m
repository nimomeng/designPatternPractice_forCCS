//
//  PrototypePattern.m
//  designPattern
//
//  Created by nimo on 2017/10/26.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "PrototypePattern.h"

@implementation PrototypePattern
- (id)copyWithZone:(NSZone *)zone
{
    PrototypePattern *cObj = [[PrototypePattern alloc] init];
    cObj.age = self.age;
    cObj.address = self.address;
    cObj.size = self.size;
//    this is for shallow clone
//    cObj.students = self.students;
//    this is for deep clone
    cObj.students = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self.students]];
    return cObj;
}
@end

//usage:
/*
 PrototypePattern *obj = [[PrototypePattern alloc] init];
 obj.address = @"test address";
 obj.size = CGSizeMake(100, 100);
 obj.age = 20;
 
 NSLog(@"1:%@,",obj);
 
 PrototypePattern *shallow_Obj = obj;
 PrototypePattern *deep_Obj = [obj copy];
 NSLog(@"s:%@",shallow_Obj);
 NSLog(@"d:%@",deep_Obj);
 */
