//
//  BridgePattern.h
//  designPattern
//
//  Created by nimo on 2017/11/1.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

//usage
// BigWrittingBrush *bigWrittingBrush = [[BigWrittingBrush alloc] init];
// [bigWrittingBrush drawPicture]
@interface BridgePattern : NSObject
@end

@interface Color: NSObject
- (void)drawColor;
@end

@interface RedColor: Color
@end

@interface GreenColor: Color
@end

@interface BlueColor: Color
@end

@interface WrittingBrush: NSObject
@property (nonatomic,strong) Color *color;

- (void)setColorToWrittingBrush:(Color *)color;
- (void)drawPicture;
@end

@interface BigWrittingBrush: WrittingBrush
@end

@interface MiddleWrittingBrush: WrittingBrush
@end

@interface SmallWrittingBrush: WrittingBrush
@end
