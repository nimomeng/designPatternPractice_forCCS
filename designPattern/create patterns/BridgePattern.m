//
//  BridgePattern.m
//  designPattern
//
//  Created by nimo on 2017/11/1.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import "BridgePattern.h"

@implementation BridgePattern
@end

@implementation WrittingBrush
- (void)setColorToWrittingBrush:(Color *)color
{
    self.color = color;
}

- (void)drawPicture
{}
@end

@implementation BigWrittingBrush
- (void)drawPicture
{
    [self tapBrushThreeTimes];
    [self.color drawColor];
}

- (void)tapBrushThreeTimes
{
//    tap three times
}
@end

@implementation MiddleWrittingBrush
- (void)drawPicture
{
    [self tapBrushTwoTimes];
    [self.color drawColor];
}

- (void)tapBrushTwoTimes
{
    //    tap two times
}
@end

@implementation SmallWrittingBrush
- (void)drawPicture
{
    [self tapBrushOneTime];
    [self.color drawColor];
}

- (void)tapBrushOneTime
{
    //    tap one time
}
@end

@implementation Color
- (void)drawColor
{}
@end

@implementation RedColor
- (void)drawColor
{
    [self useRedPigment];
}

- (void)useRedPigment
{
//    logic
}
@end

@implementation GreenColor
- (void)drawColor
{
    [self useGreenPigment];
}

- (void)useGreenPigment
{
    //    logic
}
@end

@implementation BlueColor
- (void)drawColor
{
    [self useBluePigment];
}

- (void)useBluePigment
{
    //    logic
}
@end

