//
//  SingletonPattern.h
//  designPattern
//
//  Created by nimo on 2017/10/26.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonPattern : NSObject
+ (instancetype)getInstance;
@end
