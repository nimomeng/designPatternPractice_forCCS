//
//  PrototypePattern.h
//  designPattern
//
//  Created by nimo on 2017/10/26.
//  Copyright © 2017年 nimoAndHisFriends. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrototypePattern : NSObject<NSCopying>
@property(nonatomic, assign) int age;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, assign) CGSize size;
@property(nonatomic, strong) NSArray *students;
@end
